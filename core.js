var fs = require('fs');
var request = require('request');
var _ = require('lodash');
var async = require('async');

var MongoClient = require('mongodb').MongoClient

exports.root = function(req, res) {
  MongoClient.connect( (process.env.MONGO_URL || 'mongodb://localhost/codinglove') , function(err, db) {
    if (err) {
      res.status(503).send('offline');
      return;
    }

    res.send('online');
  });
};

exports.telegram = function(req, res) {
  MongoClient.connect( (process.env.MONGO_URL || 'mongodb://localhost/codinglove') , function(err, db) {
    if (err) {
      res.status(503).send('error');
      return;
    }
    var body = _.get(req, 'body')
    res.send();

    console.log('body', body);

    if (_.has(body, 'message')) {

      if (_.has(body, 'message.text')) {

        var trimmedText = _.trim(_.get(body, 'message.text'));
        trimmedText = trimmedText.replace('@codinglovebot', '')
        if (_.isEqual(trimmedText, '/random')) {

          var query = {
            'file.bytes': {
              $lt: 50000000
            },
            'file.mime': 'image/gif'
          }

          db.collection(process.env.COLLECTION).find(query).count(function(err, totalDocuments) {
            if (err) {
              console.log(err);
              request.get('https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendMessage?chat_id='+ _.get(body, 'message.chat.id') +'&reply_to_message_id='+_.get(body, 'message.message_id')+'&text=Something went wrong!', function (error, response, body) {
                if (error) console.log(error);
                if (body) console.log(body);
              }); 
              return;
            }   

            if (!_.gte(totalDocuments, 1)) {
              request.get('https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendMessage?chat_id='+ _.get(body, 'message.chat.id') +'&reply_to_message_id='+_.get(body, 'message.message_id')+'&text=No gifs found?', function (error, response, body) {
                if (error) console.log(error);
                if (body) console.log(body);
              }); 
              return;
            }  

            db.collection(process.env.COLLECTION).find(query).limit(-1).skip(_.random(0,totalDocuments)).toArray(function(err, docs) {
              if (err) {
                console.log(err);
                return;
              } 

              if (!_.size(docs)) {
                request.get('https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendMessage?chat_id='+ _.get(body, 'message.chat.id') +'&reply_to_message_id='+_.get(body, 'message.message_id')+'&text=Something went wrong!', function (error, response, body) {
                  if (error) console.log(error);
                  if (body) console.log(body);
                }); 
                return;
              } 

              var doc = _.first(docs);

              console.log('doc', doc);

              var formData = {
                video: fs.createReadStream( (process.env.MP4_PATH || __dirname + '/mp4/') + _.get(doc, 'file.name').replace('.gif', '.mp4')),
              }
              request.get({
                url: 'https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendVideo?chat_id='+ _.get(body, 'message.chat.id') +'&reply_to_message_id='+_.get(body, 'message.message_id')+'&caption='+_.get(doc, 'post.text'), 
                formData: formData
              }, function (error, response, body) {
                if (error) console.log(error);
                if (body) console.log(body);
              }); 

            });

          });

        } else {
          request.get('https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/sendMessage?chat_id='+ _.get(body, 'message.chat.id') +'&reply_to_message_id='+_.get(body, 'message.message_id')+'&text=Not supported', function (error, response, body) {
            if (error) console.log(error);
            if (body) console.log(body);
          });  
        }

      }

    } else if (_.has(body, 'inline_query')) {
        if (_.has(body, 'inline_query.query')) {
          var query = {
            $or: [{
              'file.bytes': {
                $lt: 1000000
              },
              'file.mime': 'image/gif'
            }, {
              'file.bytes': {
                $lt: 5000000
              },
              'file.mime': 'image/jpeg'
            }]
          }

          if (_.get(body, 'inline_query.query') !== '') {
            _.extend(query, {
              $text: {
                $search: _.get(body, 'inline_query.query')
              }
            })          
          }

          console.log('query', JSON.stringify(query,null,2));
          var skip = 0;
          var next_offset = 10;
          if (_.has(body, 'inline_query.offset')) {
            skip = _.get(body, 'inline_query.offset');
          } else {
            next_offset = '30'
          }

          console.log('skip', skip);
          console.log('next_offset', next_offset);
          
          db.collection(process.env.COLLECTION).find(query).skip(skip).limit(10).sort({'post.note_count': -1}).toArray(function(err, docs) {
            if (!err) {
              var result = [];
              async.each(docs, function(doc, callback) {
                if (_.isEqual(_.get(doc, 'file.mime'), 'image/gif')) {
                  result.push({
                    type: 'mpeg4_gif',
                    id: doc._id,
                    mpeg4_url: process.env.MP4_URL+_.get(doc, 'file.name').replace('.gif','.mp4'), 
                    gif_width: 100,
                    gif_height: 100,
                    thumb_url: process.env.MP4_URL+_.get(doc, 'file.name').replace('.gif','.mp4'),
                    title: _.get(doc, 'post.text'),
                    caption: _.get(doc, 'post.text'),
                  });
                } else if (_.isEqual(_.get(doc, 'file.mime'), 'image/jpeg')) {
                  /*result.push({
                    type: 'photo',
                    id: doc._id,
                    photo_url: process.env.M+_.get(doc, 'file.name'), 
                    title: _.get(doc, 'post.text'),
                    caption: _.get(doc, 'post.text'),
                  });*/
                }


                callback();                
              }, function(err) {
                console.log('result', result);
                request.get('https://api.telegram.org/bot'+process.env.TELEGRAM_TOKEN+'/answerInlineQuery?inline_query_id='+_.get(body, 'inline_query.id')+'&results='+encodeURIComponent(JSON.stringify(result))+'&is_personal=true&next_offset='+next_offset+'&cache_time=0', function (error, response, body) {
                  if (error) console.log(error);
                  if (body) console.log(body);

                });
              })
            } else {
              console.log('error', err)
            }
          });
     


        }
      
    } else {
      res.send();
    }
  });

}
