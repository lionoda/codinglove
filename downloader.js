var request = require('request');
var async = require('async');
var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var MongoClient = require('mongodb').MongoClient
var mime = require('mime');

var mongoUrl = 'mongodb://localhost/codinglove';

var limit = 20;
var offsetStart = 0; // not working!!
var count = 0;
var prefixApi = 'https://api.tumblr.com/v2/blog/thecodinglove.tumblr.com/';
var apiKey = 'vbECHLI2kth88cRDxuMvR4kp3zsiMsdX4xFaidmHUz8zfCZeje';
var collection = 'gifs';
var folderpath = './gifs/';
var supportedExtensions = [
  '.jpg',
  '.gif'
]

var removeCollectionOnStart = false;
var deleteFolderOnStart = false;

var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

if (deleteFolderOnStart) {
  deleteFolderRecursive(folderpath);
}

if (!fs.existsSync(folderpath)) {
  fs.mkdirSync(folderpath);
}

if (offsetStart !== 0) {
  count = (offsetStart / limit);
}

MongoClient.connect(mongoUrl, function(err, db) {
  if (err) {
    console.log('no connnection with db!');
    return  
  }

  if (removeCollectionOnStart) {
    db.collection(collection).remove();
  }

  db.collection(collection).createIndex({
    'post.text': 'text' 
  })
  

  request(prefixApi+'info?api_key='+apiKey, function (error, response, body) {

    var data = JSON.parse(body);

    console.log('blog info:', _.get(data, 'response.blog'))

    var totalRequests = _.ceil(data.response.blog.posts / limit);
    
    console.log('totalRequests', totalRequests);
    
    async.whilst(function () { 
        return count < totalRequests; 
    }, function (callbackRequest) {

      var offset = (limit*count) + offsetStart;

      count++;

      request(prefixApi+'posts?api_key='+apiKey+'&limit='+limit+'&offset='+offset, function (error, response, body) {
        console.log('requested', 'posts?api_key='+apiKey+'&limit='+limit+'&offset='+offset)
        var data = JSON.parse(body);

        async.each(_.get(data, 'response.posts'), function(post, callbackPost) {

          $ = cheerio.load(_.unescape(post.body));
          var gifUrl = $('img').attr('src');
          var extension = '.'+gifUrl.substr(gifUrl.lastIndexOf('.') + 1);

          if (!_.includes(supportedExtensions, extension)) {
            return callbackPost();
          }

          var filename = _.truncate(_.snakeCase(post.title), {
            'length': 130,
            'omission': ''
          })+'_'+post.id+extension;

          var filepath = folderpath+filename;

          if (fs.existsSync(filepath)) {
            console.log('gif already exists')
            return callbackPost();
          }

          console.log('checking url', gifUrl);

          if (gifUrl.indexOf('minus') > -1) {
            console.log('weird host')
            return callbackPost();
            return;            
          }

          request.head(gifUrl, function(err, response, body) {
            
            if (err) {
              console.log('eror checking gif', gifUrl, err);
              return callbackPost();
            }
            
            if (_.get(response, 'statusCode') === 200) {
              request(gifUrl).pipe(fs.createWriteStream(filepath)).on('close', function(err, err1) {
                console.log('downloaded gif', filepath, 'from', gifUrl)
                var stats = fs.statSync(filepath);
                var fileSizeInBytes = stats['size'];

                var mimeType = mime.lookup(filepath);  

                db.collection(collection).insert({
                  _createdAt: new Date(),
                  post: {
                    id: post.id,
                    text: post.title,
                    slug: post.slug,
                    note_count: post.note_count,
                    timestamp: post.timestamp,
                    url: gifUrl
                  },
                  file: {
                    bytes: fileSizeInBytes,
                    name: filename,
                    mime: mimeType,
                  }
                }, function(err, result) {
                  return callbackPost();
                });

              });
            } else {
              console.log('gif not found on server')
              return callbackPost();
            };
          })

        }, function(error) {
          callbackRequest(null, count);
        });
        
      });
      
    }, function (err, n) {
      console.log('done', err, n);
      process.exit();
    });

  });

});