#m -rf ./mp4/*

for file in gifs/*.gif
do
  filenamewithpath=${file%%.*};
  filename=${filenamewithpath/gifs\/}
  mp4filename=mp4/$filename.mp4;
  if [ -f $mp4filename ]; then
    echo "File $mp4filename exists."
  else
    ffmpeg -i $file -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" $mp4filename
  fi

done