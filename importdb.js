var MongoClient = require('mongodb').MongoClient
var fs = require('fs');
var async = require('async');

var _ = require('lodash');

MongoClient.connect( (process.env.MONGO_URL || 'mongodb://localhost/codinglove') , function(err, db) {
  var data = fs.readFileSync('./gifs.json', 'utf8');
  data = JSON.parse(data);
  data = _.map(data, (gif) => {
    gif.search = gif.post.text
    return gif;
  })
  db.collection('gifs').drop(function(err) {
    db.collection('gifs').insert(data, function(err, docs) {
      console.log('err', err)
      process.exit()
    });
  });
});