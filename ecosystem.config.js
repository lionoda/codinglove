module.exports = {
  apps : [{
    name      : "codinglove",
    script    : "app.js",
    env: {
      PORT: 3020,
      TELEGRAM_TOKEN: '176925165:AAHS57_5dkoZkhv6DOnwBiYZrmto6iHbIS8',
      MP4_PATH: '/var/www/codinglove/mp4/',
      MP4_URL: 'https://codinglove.stevenprins.com/static/mp4/',
      COLLECTION: 'gifs'
    },
    env_production : {
      NODE_ENV: "production"
    }
  }],
  deploy : {
    production : {
      user : "stepri",
      host : "188.166.101.69",
      ref  : "origin/master",
      repo : "git@bitbucket.org:lionoda/codinglove.git",
      path : "/var/www/codinglove",
      "post-deploy" : "npm install && pm2 startOrRestart ecosystem.config.js --env production"
    },
  }
}
