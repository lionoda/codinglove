var express = require('express');
var path = require('path');
var fs = require('fs');
var morgan = require('morgan');
var _ = require('lodash');

var logger = require('morgan');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient

var core = require('./core');

MongoClient.connect( (process.env.MONGO_URL || 'mongodb://localhost/codinglove') , function(err, db) {
  if (err) {
    console.log(err);
  } else {
    db.collection('gifs').createIndex( { search: "text" } , function read(err) {
      if (err) console.log('error setup index')
    })

    fs.readFile(__dirname+'/gifs.json', function read(err, data) {
      if (err) {
        console.log('readfile',err);
        return;
      }

      data = JSON.parse(data);

      db.collection(process.env.COLLECTION).remove();
      db.collection(process.env.COLLECTION).insertMany(data, function(err, result) {
        console.log('insertMany', err, _.size(result.ops))
      });
    });

  }
});

var app = express();
app.use(morgan(':date[web] - :method :url :status :res[content-length] - :response-time ms'))
app.disable('x-powered-by');
app.disable('etag');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.post('/', core.root);

app.use('/static/gifs', express.static(path.join( (process.env.GIFS_PATH || __dirname + '/gifs/') )));
app.use('/static/mp4', express.static(path.join( (process.env.MP4_PATH || __dirname + '/mp4/') )));

app.post('/webhook/telegram', core.telegram);

app.use(function(req, res, next) {
  res.status(404).send()
});

app.listen( (process.env.PORT || 8080), function(){
  console.log('Server started at ' + (process.env.PORT || 8080));
});

module.exports = app;

  
