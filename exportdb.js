var MongoClient = require('mongodb').MongoClient
var fs = require('fs');
var async = require('async');

var _ = require('lodash');
function map(doc, callback) {

  if (_.has(doc, 'file.filename')) {
    doc.file.name = _.get(doc, 'file.filename')
    delete doc.file['filename'];     
  }

  delete doc['_id'];


  callback(null, doc);
    
}
MongoClient.connect( (process.env.MONGO_URL || 'mongodb://localhost/codinglove') , function(err, db) {

  db.collection('gifs').find({}).toArray(function(err, docs) {

    async.map(docs, map, function(err, docs) {
     fs.writeFile('gifs.json', JSON.stringify(docs, null, 2), function(err) {

      process.exit();

    });   
    });


  });


});