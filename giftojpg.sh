rm -rf ./jpg/*

for file in mp4/*.mp4
do
  filenamewithpath=${file%%.*};
  filename=${filenamewithpath/mp4\/}
  jpgfilename=jpg/$filename.jpg;
  if [ -f $jpgfilename ]; then
    echo "File $jpgfilename exists."
  else
    ffmpeg -ss 3 -i $file -vf "select=gt(scene\,0.4)" -frames:v 1 -vsync vfr -vf fps=fps=1/600 jpg/$filename-%02d.jpg;

  fi

done