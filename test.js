var MongoClient = require('mongodb').MongoClient
var _ = require('lodash');

MongoClient.connect( (process.env.MONGO_URL || 'mongodb://localhost/codinglove') , function(err, db) {

  db.collection('gifs').count(function(err, totalDocuments) {
    
    console.log(totalDocuments);
    
    db.collection('gifs').find().limit(-1).skip(_.random(0,totalDocuments)).toArray(function(err, docs) {
      console.log(_.first(docs));
      process.exit();
    });

  });





});